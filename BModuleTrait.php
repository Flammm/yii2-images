<?php

namespace flammm\yii2images;

use yii\base\Exception;

trait ModuleTrait
{

    private $_module;

    protected function getModule()
    {
        if ($this->_module == null) {
            $this->_module = \Yii::$app->getModule('images');
        }

        if(!$this->_module){
            throw new Exception("Images module not found, may be you didn't add it to your config?");
        }

        return $this->_module;
    }
}