<?php

use yii\db\Schema;

class m140622_111540_create_image_table extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('{{%image}}', [
            'id' => Schema::TYPE_PK,
            'file_path' => Schema::TYPE_STRING.' NOT NULL',
            'item_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'model_name' => Schema::TYPE_STRING.' NOT NULL',
            'url_alias' => Schema::TYPE_STRING.' NOT NULL',
            'active' => Schema::TYPE_INTEGER.' NOT NULL DEFAULT 1',
            'sorter' => Schema::TYPE_INTEGER.' NOT NULL',
        ]);

        $this->createIndex('idx_image_active', '{{%image}}', 'active');
        $this->createIndex('idx_image_sorter', '{{%image}}', 'sorter');
    }

    public function down()
    {
        $this->dropTable('{{%image}}');
    }
}
