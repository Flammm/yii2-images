<?php

namespace flammm\yii2images;

use flammm\yii2images\models\Placeholder;

class Module extends \yii\base\Module
{

    public $imagesStorePath = '@app/web/uploads/imgs';
    public $imagesCachePath = '@app/web/uploads/imgs_cache';
    public $graphicsLibrary = 'GD';
    public $placeholderPath;
    public $waterMark = false;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }


    public function getImageSubDir($model)
    {
        $modelName = \yii\helpers\Inflector::tableize($this->getShortClass($model));

        $modelDir = $modelName . '/' . $model->{$model->idAttribute};
        return $modelDir;
    }

    public  function getShortClass($obj)
    {
        $className = get_class($obj);
//        vd($className);
        if (preg_match('@\\\\([\w]+)$@', $className, $matches)) {
            $className = $matches[1];
        }

        return $className;
    }


    public function getStorePath()
    {
        return \Yii::getAlias($this->imagesStorePath);
    }

    public function getPlaceholder(){

        if($this->placeholderPath){
            return new Placeholder();
        } else {
            return null;
        }
    }

}
