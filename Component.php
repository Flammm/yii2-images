<?php

namespace flammm\yii2images;


class Component extends \yii\base\Object {

    /**
     * Images store path
     * @var string
     */
    public $imagesStorePath = '@app/web/uploads/imgs';

    /**
     * Images cache store path
     * @var string
     */
    public $imagesCachePath = '@app/web/uploads/imgs_cache';

    /**
     * Graphic library
     * @var string
     */
    public $graphicLibrary = 'GD';

    /**
     * Placeholder path
     * @var string
     */
    public $placeholderPath = '@app/web/uploads/imgs/placeholder.jpg';


    /**
     * Placeholder path
     * @var string
     */
    public $watermark = false;

//    public function init() {
//
//    }

    public function getStorePath()
    {
        return \Yii::getAlias($this->imagesStorePath);
    }

    public function getCachePath()
    {
        return \Yii::getAlias($this->imagesCachePath);
    }

    public function getPlaceholder(){

        if($this->placeholderPath){
            return new \flammm\yii2images\models\Placeholder();
        } else {
            return null;
        }
    }

    /**
     *
     * Parses size string
     * For instance: 400x400, 400x, x400
     *
     * @param $notParsedSize
     * @return array|null
     */
    public function parseSize($notParsedSize)
    {
        $sizeParts = explode('x', $notParsedSize);
        $part1 = (isset($sizeParts[0]) and $sizeParts[0] != '');
        $part2 = (isset($sizeParts[1]) and $sizeParts[1] != '');
        if ($part1 && $part2) {
            if (intval($sizeParts[0]) > 0
                &&
                intval($sizeParts[1]) > 0
            ) {
                $size = [
                    'width' => intval($sizeParts[0]),
                    'height' => intval($sizeParts[1])
                ];
            } else {
                $size = null;
            }
        } elseif ($part1 && !$part2) {
            $size = [
                'width' => intval($sizeParts[0]),
                'height' => null
            ];
        } elseif (!$part1 && $part2) {
            $size = [
                'width' => null,
                'height' => intval($sizeParts[1])
            ];
        } else {
            throw new Exception('Something wrong with format size string');
        }

        return $size;
    }

}
