<?php

namespace flammm\yii2images;

use Yii;
use flammm\yii2images\models\Image;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;

class ImageBehavior extends \yii\base\Behavior
{

    public $idAttribute = 'id';
    public $modelClass = '\flammm\yii2images\models\Image';
    public $createAliasMethod = false;

    public $component = 'images';

    public $attribute = 'images';


    public function init()
    {
        $this->component = \yii\di\Instance::ensure($this->component, Component::className());

        if ($this->attribute === null) {
            throw new nvalidConfigException('Attribute not set');
        }
    }

    public function attachImage($newImage)
    {

        if (!$this->owner->{$this->idAttribute}) {
            throw new \Exception($this->owner->classname() . ' must have an Id when you attach image');
        }

        if ($newImage instanceof UploadedFile) {
            $sourceFile = $newImage->tempName;
            $imageExt = $newImage->extension;
        } else {
            $sourceFile = $newImage;
            $imageExt = strtolower(pathinfo($newImage, PATHINFO_EXTENSION));
        }

        if (!file_exists($sourceFile)) {
            throw new \Exception('Source file doesn\'t exist: ' . $sourceFile);
        }

        $newImageName = substr(sha1(microtime(true) . $sourceFile), 4, 12) . '.' . $imageExt;
        $newImageSubdir = $this->getImageSubDir();
        $storePath = $this->component->getStorePath();


        $destFolder = FileHelper::normalizePath($storePath . '/'
            . $newImageSubdir);
        $destFile = $destFolder . DIRECTORY_SEPARATOR
            . $newImageName;

        FileHelper::createDirectory($destFolder,
            0775, true);

        if (!copy($sourceFile, $destFile)) {
            throw new \Exception('Failed to copy file from ' . $sourceFile . ' to ' . $destFile);
        }

        $image = new $this->modelClass;
        $image->item_id = $this->owner->{$this->idAttribute};
        $image->file_path = $newImageSubdir . '/' . $newImageName;
        $image->model_name = $this->getShortClass();
        $image->sorter = $this->getMaxSorter() + 1;

        $image->url_alias = $this->getAlias($image);

        if (!$image->save()) {
            return false;
        } else {
            if (count($image->getErrors()) > 0) {
                $errors = array_shift($image->getErrors());

                unlink($destFile);
                throw new \Exception(array_shift($errors));
            }
            return $image;
        }
    }

    /**
     * Returns model images
     * First image alwats must be main image
     * @return array|\yii\db\ActiveRecord[]
*/
    public function getImages($usePlaceholder = true)
    {
        $query = $this->imageQuery();
        $images = $query->all();
        if ((empty($images)) && ($usePlaceholder) && ($this->component->getPlaceholder())) {
            $images[] = $this->component->getPlaceholder();
        }

        return $images;
    }

    public function getImage($pos = null, $usePlaceholder = true)
    {
        $query = $this->imageQuery();
        if($pos) {
            $query->andWhere(['sorter' => $pos]);
        }
        $img = $query->one();

        if (!$img && $usePlaceholder) {
            return $this->component->getPlaceholder();
        }

        return $img;
    }

    private function imageQuery()
    {
        $query = Image::find()
            ->where([
                'item_id' => $this->owner->{$this->idAttribute},
                'model_name' => $this->getShortClass(),
            ])
            ->orderBy(['sorter' => SORT_ASC]);

        return $query;
    }

    /**
     * Removes concrete model's image
     * @param Image $img
     * @throws \Exception
     */
    public function removeImage(Image $img)
    {
       $img->clearCache();

        $storePath = $this->component->getStorePath();

        $fileToRemove = $storePath . DIRECTORY_SEPARATOR . $img->file_path;
        if (preg_match('@\.@', $fileToRemove) and is_file($fileToRemove)) {
            unlink($fileToRemove);
        }
        $img->delete();
    }

    /**
     * Remove all model images
     */
    public function removeImages()
    {
        $images = $this->owner->getImages();
        if (count($images) < 1) { return true; }

        foreach ($images as $image) {
            $this->owner->removeImage($image);
        }

        $dirToRemove = $this->component->getCachePath() . DIRECTORY_SEPARATOR . $this->getImageSubDir();
//        vd(FileHelper::normalizePath($dirToRemove));
        FileHelper::removeDirectory(FileHelper::normalizePath($dirToRemove));
    }

    private function getAlias()
    {
        $aliasWords = $this->getAliasString();
        $maxSorter = $this->getMaxSorter();

        return $aliasWords . '-' . intval($maxSorter + 1);
    }

    // TODO Closure for get alias string
    private function getAliasString()
    {
        return substr(sha1(microtime()), 3, 20);
    }

    private function getMaxSorter()
    {
        return Yii::$app->db->createCommand('SELECT MAX(sorter) FROM {{%image}} WHERE `model_name` = "' . $this->getShortClass() . '" AND `item_id` = ' . $this->owner->{$this->idAttribute})->queryScalar();
    }

    public function getImageSubDir()
    {
        $modelName = \yii\helpers\Inflector::tableize($this->getShortClass($this->owner));

        $modelDir = $modelName . '/' . $this->owner->{$this->idAttribute};
        return $modelDir;
    }

    public function getShortClass()
    {
        $className = get_class($this->owner);
//        vd($className);
        if (preg_match('@\\\\([\w]+)$@', $className, $matches)) {
            $className = $matches[1];
        }

        return $className;
    }

    public function getPlaceholder()
    {

        if ($this->component->placeholderPath) {
            return new Placeholder();
        } else {
            return null;
        }
    }

    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }


    public function beforeValidate($event)
    {
        $model = $this->owner;
        if ($model->{$this->attribute} instanceof UploadedFile) {
            $file = $model->{$this->attribute};
        } else {
            $file = UploadedFile::getInstances($model, $this->attribute);
        }
        vd($file);
//        if ($file && $file->name) {
//            vd($file->name);
//            $model->{$this->fileAttribute} = $file;
//            $validator = Validator::createValidator('file', $model, $this->fileAttribute,  [
//                'types'=>$this->fileTypes,
//            ]);
//            $validator->validateAttribute($model, $this->fileAttribute);
//        }
    }

//    public function beforeValidate() {
//        if (!empty($this->owner[$this->attribute])) {
////            $ids = (array) $this->owner[$this->attribute];
//            vd($this->owner[$this->attribute]);
//        }
//    }

}


