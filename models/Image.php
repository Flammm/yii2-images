<?php

namespace flammm\yii2images\models;

use yii\base\Exception;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\db\ActiveRecord;

class Image extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%image}}';
    }

    public function rules()
    {
        return [
            [['file_path', 'item_id', 'model_name', 'url_alias'], 'required'],
            [['item_id'], 'integer'],
            [['file_path', 'url_alias', 'model_name'], 'string', 'max' => 255],
            [['active', 'sorter'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_path' => 'Путь',
            'item_id' => 'Id модели',
            'is_main' => 'Главное',
            'model_name' => 'Model Name',
            'url_alias' => 'Url Alias',
        ];
    }


    public function getUrl($size = false, $altRoute = false )
    {
        $urlSize = ($size) ? '_' . $size : '';

        $route = ($altRoute === false) ? \Yii::$app->controller->id : $altRoute;
        $url = Url::toRoute([$route.'/image', 'id'=>$this->item_id, 'ref'=>$this->url_alias.$urlSize]);

        return $url;
    }

    public function getPath($size = false)
    {
        $urlSize = ($size) ? '_' . $size : '';
        $base = \Yii::$app->images->getCachePath();
        $cacheDir = $this->getCacheDir();

        $origin = $this->getPathToOrigin();

        $filePath = $base . DIRECTORY_SEPARATOR .
            $cacheDir . DIRECTORY_SEPARATOR . $this->url_alias . $urlSize . '.' . pathinfo($origin, PATHINFO_EXTENSION);
        if(!file_exists($filePath)){
            $this->createVersion($origin, $size);

            if(!file_exists($filePath)) {
                throw new \Exception('Problem with image creating');
            }
        }

        return $filePath;
    }

    public function getContent($size = false, $base64 = false)
    {
        return ($base64) ? base64_encode(file_get_contents($this->getPath($size))) : file_get_contents($this->getPath($size));
    }

    protected function getSubDur()
    {
        return \yii\helpers\Inflector::tableize($this->model_name) . '/' . $this->item_id;
    }

    public function getPathToOrigin()
    {
        $base = \Yii::$app->images->getStorePath();
        $filePath = $base . DIRECTORY_SEPARATOR . $this->file_path;

        return $filePath;
    }

    public function createVersion($imagePath, $sizeString = false)
    {
        if(strlen($this->url_alias)<1){
            throw new \Exception('Image without url alias');
        }

        $cachePath = \Yii::$app->images->getCachePath();
        $cacheDir = $this->getCacheDir();
        $fileExtension =  pathinfo($this->file_path, PATHINFO_EXTENSION);

        if($sizeString) {
            $sizePart = '_' . $sizeString;
        } else {
            $sizePart = '';
        }

        $pathToSave = $cachePath . '/' . $cacheDir . '/' . $this->url_alias . $sizePart . '.' . $fileExtension;

        FileHelper::createDirectory(dirname($pathToSave), 0777, true);


        if($sizeString) {
            $size =  \Yii::$app->images->parseSize($sizeString);
        } else {
            $size = false;
        }

        if(\Yii::$app->images->graphicLibrary == 'Imagick'){
            $image = new \Imagick($imagePath);
            $image->setImageCompressionQuality(100);

            if($size){
                if($size['height'] && $size['width']){
                    $image->thumbnailImage($size['width'], $size['height'], true);
                } elseif($size['height']){
                    $image->thumbnailImage(0, $size['height']);
                } elseif($size['width']){
                    $image->thumbnailImage($size['width'], 0);
                } else {
                    throw new \Exception('Something wrong with format size string');
                }
            }

            $image->writeImage($pathToSave);
        } else {
            $image = new \abeautifulsite\SimpleImage($imagePath);
            if($size){
                if($size['height'] && $size['width']){
                    $image->best_fit($size['width'], $size['height']);
                } elseif($size['height']){
                    $image->fit_to_height($size['height']);
                } elseif($size['width']){
                    $image->fit_to_width($size['width']);
                } else {
                    throw new \Exception('Something wrong with format size string');
                }
            }

            //WaterMark
            if(\Yii::$app->images->watermark){
                if(!file_exists(Yii::getAlias(\Yii::$app->images->watermark))){
                    throw new Exception('WaterMark not detected!');
                }

                $wmMaxWidth = intval($image->get_width()*0.4);
                $wmMaxHeight = intval($image->get_height()*0.4);
                $watermarkPath = Yii::getAlias(\Yii::$app->images->watermark);
                $watermark = new \abeautifulsite\SimpleImage($watermarkPath);
                if(($watermark->get_height() > $wmMaxHeight) || ($watermark->get_width() > $wmMaxWidth)) {
                    $watermarkPath = \Yii::$app->images->getCachePath().DIRECTORY_SEPARATOR.
                        pathinfo(\Yii::$app->images->watermark)['filename'].
                        $wmMaxWidth.'x'.$wmMaxHeight.'.'.
                        pathinfo(\Yii::$app->images->watermark)['extension'];

                    //throw new Exception($waterMarkPath);
                    if(!file_exists($watermarkPath)) {
                        $watermark->fit_to_width($wmMaxWidth);
                        $watermark->save($watermarkPath, 100);
                        if(!file_exists($watermarkPath)) {
                            throw new Exception('Cant save watermark to ' . $watermarkPath.'!!!');
                        }
                    }
                }
                $image->overlay($watermarkPath, 'bottom right', .5, -10, -10);
            }
            $image->save($pathToSave, 100);
        }

        return $image;
    }

    public function getCacheDir() {
        return $this->getSubDur() . DIRECTORY_SEPARATOR . $this->sorter;
    }

    public function clearCache(){
        $cacheDir = $this->getCacheDir();
        $dirToRemove = \Yii::$app->images->getCachePath() . DIRECTORY_SEPARATOR . $cacheDir;
        FileHelper::removeDirectory($dirToRemove);

        return true;
    }

}
